#pragma once
#include <vector>

using namespace std;

class Game
{
private:
	vector<int> m_scores;
	int m_times[21];
public:
	Game(void);
	~Game(void);


	void Roll(int pins)
	{
		int currentBall = m_scores.size();

		m_scores.push_back(pins);
		if(pins == 10 && currentBall % 2 == 0) // strike
		{
			m_scores.push_back(0);
		}
		
	}

	int GetScore() 
	{
		for (int i = 0; i < m_scores.size(); i++)
		{
			int pins = m_scores[i];
			if(pins == 10 && i % 2 == 0 && i < 18) // strike
			{
				i++;
			}
		}

		int sum = 0;
		for (int i = 0; i < m_scores.size(); i++)
		{
			if(i <= 14 &&  i % 2 == 0 && m_scores[i] == 10) { // strike
				sum += m_scores[i+2] + m_scores[i+4];
			}
			if(i == 16 && m_scores[i] == 10) {
				sum += m_scores[i+2] + m_scores[i+3];
			}
			if(i == 18 && m_scores[i] == 10) {
				sum += m_scores[i+1] + m_scores[i+2];
			}
			sum += m_scores[i];
		}
		return sum;
	}
};

// 1     2    3
// 10 0, 10 0, 0 10
// 0  1  2 3  4 5
// 0  0  1 1  2 2
